import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { unicorn } from '../functions/unicorn.function';

export function UnicornOnError(max: number = 1) {
  // tslint:disable-next-line:only-arrow-functions
  return function(
    target: any,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<(...args: any[]) => Observable<any>>
  ) {
    const originalMethod = descriptor.value;
    descriptor.value = function() {
      return originalMethod.apply(this)
        .pipe(
          catchError((err, caught) => {
            unicorn(max).subscribe();
            return throwError('Unicorn error: ' + err);
          })
      );
    };
    return descriptor;
  };
}
