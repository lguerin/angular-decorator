import { Component } from '@angular/core';
import { MyService } from './services/my.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-decorator';

  constructor(private myService: MyService) {}

  ok() {
    this.myService.service1().subscribe((result) => {
      console.log('>> %o', result);
    });
  }

  error() {
    this.myService.service2().subscribe(() => {}, (err) => { console.log('>> %o', err); });
  }
}
