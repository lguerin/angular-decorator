/* tslint:disable:no-string-literal */
import { tap } from 'rxjs/operators';
import { Observable, range } from 'rxjs';

export function unicorn(max: number): Observable<any> {
  return range(1, max).pipe(
    tap(() => window['cornify_add']())
  );
}

