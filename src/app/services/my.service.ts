import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { UnicornOnError } from '../decorators/unicorn.decorator';

@Injectable({
  providedIn: 'root'
})
export class MyService {

  constructor() { }

  @UnicornOnError()
  service1(): Observable<any> {
    return of('Service call: ok');
  }

  @UnicornOnError(8)
  service2(): Observable<any> {
    return throwError('Error occured on service2 call');
  }
}
